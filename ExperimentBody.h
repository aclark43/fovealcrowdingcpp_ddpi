#pragma once

#include "emil/EMIL.hpp"
#include "CVertexObject.hpp"
#include "Pest.h"
#include <fstream>

class BadConversion : 
public std::runtime_error 
{
public:
BadConversion(std::string const& s)
: std::runtime_error(s)
{ }
};
inline std::string stringify(double x)
 {
  std::ostringstream o;
 if (!(o << x))
  throw BadConversion("stringify(double)");
return o.str();
} 

//using namespace std;
class ExperimentBody: public CExperiment
{
public:
	ExperimentBody(int pxWidth, int pxHeight, 
		int RefreshRate, CCfgFile* Params);

	/// Standard event handlers
	void initialize();
	void finalize();
	void eventRender(unsigned int FrameCount, CEOSData* Samples);	

	// added by Janis /////
	void eventJoypad();
	void eventKeyboard(unsigned char key, int x, int y);
	//////////////////////
	
private:

	enum STATE 
	{
		STATE_LOADING,
		STATE_TESTCALIBRATION,
		STATE_FIXATION,
		STATE_CUE,
		STATE_TARGET,
		STATE_RESPONSE,
	};

	STATE m_state;

	void gotoFixation();
	void saveData();
	void loadProgress();
	void loadSession();// load session master list


	int ScreenDistance;
	float pixelAngle;
	float pixelAngle0;
	float magnificationFactor = 1.0f;
	float isPest;
	float fixedContrast;
	float fixedTargetStrokewidth;
	// Configuration file
	CCfgFile* m_paramsFile;


	// Stimuli for the test calibration
	//CSolidPlane* m_redcross;
	//CSolidPlane* m_whitecross;
	CImagePlane* m_redcross;
	CImagePlane* m_whitecross;
	CSolidPlane* m_fixation;
	CSolidPlane* m_fixationEcc;
	CImagePlane* m_target;
	vector<CImagePlane*> m_flankers;
	vector<int>TargetEccentricityVector;

	CImagePlane* m_arcs;

	// timers
	CTimer m_timer;
	CTimer m_timerCheck;
	CTimer m_timerExp;
	CTimer m_timerfixation;
	CTimer m_timercue;
	CTimer m_timerisi;
	CTimer m_timerhold;
	CTimer m_timertarget;

	float ResponseTime;

	// parameters for current trial
	//int screenDist; //06142019 AMC
	int TargetEccentricity;
	int TargetEccentricityIndex;
	int TargetOrientation;
	int TargetStrokewidth; // in pixels
	int FlankerOrientations;

	vector<float> xFlankers;
	vector<float> yFlankers;

	int nResponses;
	int nHits;
	int Increment;
	int m_numCompleted;
	int m_numTestCalibration;
	int m_numSession;
	int ResponseFinalize;
	int TestCalibration;
	int WAIT_RESPONSE;
	int gate;
	int TrialNumber;
	int debug;

	int Correct;
	int Response;
	//new one
	int Conditions;

	float TimeTargetON;
	float TimeTargetOFF;
	float TimeCueON;
	float TimeCueOFF;
	float TimeFixationON;
	float TimeFixationOFF;
	float Dist;
	float X;
	float Y;
	float Xstab;
	float Ystab;
	float xPos;
	float yPos;
	float xshift;
	float yshift;
	//float magnificationFactor = 1.0f; //06142019 AMC


	bool SHOW_TARGET;
	int targetGray;
	int unstab;
	int uncrowded;
	int nRecal;
	int nFixat;
	int nTrials;
	int flankerType;
	float flankerDist;
	int fixationSize;
	int nasal;
	

	Pest* pest;
	float initLevel;
	float pestLevel;
};