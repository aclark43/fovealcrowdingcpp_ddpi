#ifndef _CVERTEXOBJECT_H
#define _CVERTEXOBJECT_H

#include <emil/CGraphicObject.hpp>
#include <emil/COGLEngine.hpp>
#include <vector>

/// The basic solid plane primitive
/** CSolidPlane describes a class of solid color objects that are drawn by the
current experiment every frame. */

typedef float VertexType;
typedef std::vector<VertexType> VertexArray;

class CVertexObject : public CGraphicObject
{
public:

	/// Default constructor
	CVertexObject();

	/// Construct a vertex object with solid-color
	/** @param R Red component
	@param G Green component
	@param B Blue component */
	CVertexObject(unsigned char R, unsigned char G, unsigned char B);

	/// Construct a vertex object with solid-color
	/** @param Color Structure containing the RGB components of the color */
	CVertexObject(EIS_RGB Color);

	/// Default destructor
	~CVertexObject();

	/// Draw this CVertexObject
	/** @param Override Force the object rendering indipendently from the current status
	@throws xDisplay if the plane cannot be rendered */
	void _renderObject();

	/// Set the solid color of this CVertexObject
	/** @param R Red component
	@param G Green component
	@param B Blue component */
	void setColor(unsigned char R, unsigned char G, unsigned char B)
	{
		m_color.r = R;
		m_color.g = G;
		m_color.b = B;
	}

	/// Set the solid color of this CVertexObject
	/** @param Color Structure containing the RGB components of the color */
	void setColor(EIS_RGB Color)
	{
		m_color = Color;
	}

	/// Set the size of the graphic object in pixels
	virtual void pxSetSize(float pxWidth, float pxHeight);

	void setVertices(VertexArray verticesX, VertexArray verticesY);
	void setVertices(VertexType* verticesX, VertexType* verticesY, unsigned int nVertices);
	void setVerticesFromRawData(VertexArray verticesX, VertexArray verticesY);
	void setVerticesFromRawData(VertexType* verticesX, VertexType* verticesY, unsigned int nVertices);
	void removeAllVertices();

protected:
	EIS_RGB m_color;

private:
	void compileVertices();
	void normalizeVertices();
	void setVerticesImpl(VertexArray verticesX, VertexArray verticesY, bool normalize);

	GLuint		m_glListIdx = 0;
	VertexArray m_verticesX;
	VertexArray m_verticesY;
	bool		m_nomarlize = true;
};

#endif // _CSOLIDPLANE_H