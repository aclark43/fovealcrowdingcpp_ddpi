#include "stdafx.h"
#include <emil/libc_common.hpp>
#include <emil/CEnvironment.hpp>
#include <emil/CExceptions.hpp>
#include <algorithm>
#include "CVertexObjectExceptions.hpp"
#include "CVertexObject.hpp"

///////////////////////////////////////////////////////////////////////////////////
// Default constructor
CVertexObject::CVertexObject()
	: CGraphicObject()
{
	// Create a standard blue square
	m_color = EIS_RGB(0, 0, 255);

	// Set it's default dimensions to 20px by 20px
	pxSetSize(20, 20);
}

///////////////////////////////////////////////////////////////////////////////////
CVertexObject::CVertexObject(unsigned char R, unsigned char G, unsigned char B)
	: CGraphicObject()
{
	// Create a solid color square
	m_color = EIS_RGB(R, G, B);

	// Set it's default dimensions to 20px by 20px
	pxSetSize(20, 20);
}
///////////////////////////////////////////////////////////////////////////////////
CVertexObject::CVertexObject(EIS_RGB Color)
	: CGraphicObject()
{
	// Create a solid color square
	m_color = Color;

	// Set it's default dimensions to 20px by 20px
	pxSetSize(20, 20);
}
///////////////////////////////////////////////////////////////////////////////////
CVertexObject::~CVertexObject()
{
	if (m_glListIdx > 0) glDeleteLists(m_glListIdx, 1);
}
///////////////////////////////////////////////////////////////////////////////////
// Draw this CVertexObject
void CVertexObject::_renderObject()
{
	// If this plane is not scheduled to be shown
	if (!m_show) return;

	glPushMatrix();

	// Apply final translation and rotation of the polygon
	glTranslatef(m_pxX, m_pxY, 0.0f);
	glRotatef(m_rotation, 0.0f, 0.0f, 1.0f);

	// Draw the polygon with the specified color
	glColor3ub(m_color.r, m_color.g, m_color.b);
	if (m_glListIdx) glCallList(m_glListIdx);

	glPopMatrix();

	// Verify if any OpenGL occurred
	GLenum OGLError = glGetError();
	if (OGLError != GL_NO_ERROR)
		throw xVertex(xVertex::ERR_OGL,
		"CVertexObject::renderObject",
		reinterpret_cast<const char*>(gluErrorString(OGLError)));
}
///////////////////////////////////////////////////////////////////////////////////
void CVertexObject::normalizeVertices()
{
	auto Xmax = std::max_element(m_verticesX.begin(), m_verticesX.end());
	auto Xmin = std::min_element(m_verticesX.begin(), m_verticesX.end());
	float Xmean = (*Xmax + *Xmin) / 2.f;
	float Xscale = 1.f / (*Xmax - *Xmin);

	auto Ymax = std::max_element(m_verticesY.begin(), m_verticesY.end());
	auto Ymin = std::min_element(m_verticesY.begin(), m_verticesY.end());
	float Ymean = (*Ymax + *Ymin) / 2.f;
	float Yscale = 1.f / (*Ymax - *Ymin);

	for (int i = 0; i < m_verticesX.size(); i++)
	{
		m_verticesX[i] = (m_verticesX[i] - Xmean) * Xscale;
		m_verticesY[i] = (m_verticesY[i] - Ymean) * Yscale;
	}
	m_nomarlize = true;
}
///////////////////////////////////////////////////////////////////////////////////
// Set Vertices
void CVertexObject::setVertices(
	VertexType* verticesX,
	VertexType* verticesY,
	unsigned int nVertices)
{
	setVertices(
		VertexArray(verticesX, verticesX + nVertices),
		VertexArray(verticesY, verticesY + nVertices));
}
///////////////////////////////////////////////////////////////////////////////////
// Set Vertices
void CVertexObject::setVertices(VertexArray verticesX, VertexArray verticesY)
{
	setVerticesImpl(verticesX, verticesY, true);
}
///////////////////////////////////////////////////////////////////////////////////
// Set Vertices
void CVertexObject::setVerticesFromRawData(
	VertexArray verticesX, VertexArray verticesY)
{
	setVerticesImpl(verticesX, verticesY, false);
}
///////////////////////////////////////////////////////////////////////////////////
// Set Vertices
void CVertexObject::setVerticesFromRawData(
	VertexType* verticesX, VertexType* verticesY, unsigned int nVertices)
{
	setVerticesFromRawData(
		VertexArray(verticesX, verticesX + nVertices),
		VertexArray(verticesY, verticesY + nVertices));
}
///////////////////////////////////////////////////////////////////////////////////
// Set Vertices
void CVertexObject::setVerticesImpl(
	VertexArray verticesX, VertexArray verticesY, bool normalize)
{
	unsigned int size = verticesX.size();
	unsigned int ySize = verticesY.size();
	if (size != ySize)
	{
		throw xVertex(xVertex::ERR_SIZE_TOO_LESS, "CVertexObject::setVertices");
	}

	if ((size < 3) || (ySize < 3))
	{
		throw xVertex(xVertex::ERR_SIZE_NOT_MATCH, "CVertexObject::setVertices");
	}

	m_verticesX.resize(size);
	m_verticesY.resize(size);
	for (int i = 0; i < size; i++)
	{
		m_verticesX[i] = verticesX[i];
		m_verticesY[i] = verticesY[i];
	}
	m_nomarlize = normalize;
	compileVertices();
}
///////////////////////////////////////////////////////////////////////////////////
// Remove all Vertices
void CVertexObject::compileVertices()
{
	// compile the display list, store a triangle in it
	if (m_glListIdx > 0) glDeleteLists(m_glListIdx, 1);
	int vSize = m_verticesX.size();
	if (vSize == 0) return;
	if (vSize % 3 != 0) throw(xVertex(xVertex::ERR_SIZE_NOT_MATCH, "CVertexObject::compileVertices"));

	m_glListIdx = glGenLists(1);
	glNewList(m_glListIdx, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	if (m_nomarlize)
	{
		for (int vId = 0; vId < vSize; vId++)
		{
			glVertex2f(m_verticesX[vId] * m_pxWidth, m_verticesY[vId] * m_pxHeight);
		}
	}
	else
	{
		for (int vId = 0; vId < vSize; vId++)
		{
			glVertex2f(m_verticesX[vId], m_verticesY[vId]);
		}
	}
	glEnd();
	glEndList();

	// Verify if any OpenGL occurred
	GLenum OGLError = glGetError();
	if (OGLError != GL_NO_ERROR)
		throw xVertex(xVertex::ERR_OGL,
		"CVertexObject::compileVertices",
		reinterpret_cast<const char*>(gluErrorString(OGLError)));
}
///////////////////////////////////////////////////////////////////////////////////
// Remove all Vertices
void CVertexObject::removeAllVertices()
{
	m_verticesX.clear();
	m_verticesY.clear();

	if (m_glListIdx > 0)
	{
		glDeleteLists(m_glListIdx, 1);
		m_glListIdx = 0;
	}
}
///////////////////////////////////////////////////////////////////////////////////
// Remove all Vertices
void CVertexObject::pxSetSize(float pxWidth, float pxHeight)
{
	m_pxWidth = pxWidth;
	m_pxHeight = pxHeight;
	if (!m_nomarlize) normalizeVertices();
	compileVertices();
}