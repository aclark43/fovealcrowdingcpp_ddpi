#include "stdafx.h"
#include "ExperimentBody.h"
#include "FovealAcuity.h"
#include <math.h>
//#include "ExFun.h"

///////////////////////////////////////////////////////////////////////////////////
ExperimentBody::ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params) :
CExperiment(pxWidth, pxHeight, RefreshRate),

m_paramsFile(Params)

{
	setExperimentName("FovealAcuity");
	m_paramsFile = Params;
	CMath::srand(time(NULL));
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::initialize()
{
	CStabilizer::Instance()->enableSlowStabilization(false);

	initLevel = m_paramsFile->getFloat(CFG_PESTINIT); // pest initial strokewidth level

													  //Load session data
								
	loadProgress();
	loadSession();


	//set target eccentricity for the seccsion

	magnificationFactor = m_paramsFile->getFloat(CFG_MAGFACTOR);
	pixelAngle0 = CConverter::Instance()->px2a(1.f);
	pixelAngle = magnificationFactor * pixelAngle0; // arcmin per pixel

	TargetEccentricity = round(m_paramsFile->getInteger(CFG_TARGET_ECCENTRICITY) / pixelAngle);
	uncrowded = m_paramsFile->getInteger(CFG_UNCROWDED);
	nasal = m_paramsFile->getInteger(CFG_NASAL);

	CEnvironment::Instance()->outputMessage("Target Eccentricity is %i pixels, or %i in arcmin", TargetEccentricity, m_paramsFile->getInteger(CFG_TARGET_ECCENTRICITY));

	ScreenDistance = CEnvVariables::Instance()->getInteger(CFG_N_SCREEN_DISTANCE);

	int fixationSize = m_paramsFile->getInteger(CFG_FIXATION_SIZE)/pixelAngle;
	m_fixation = addObject(new CSolidPlane(255, 255, 255));
	m_fixation->pxSetSize(fixationSize, fixationSize);
	m_fixation->pxSetPosition(0, 0);
	m_fixation->hide();

	// arcs - make these bigger than 800?
	m_arcs = addObject(new CImagePlane("images/arcs.tga"));
	m_arcs->enableTrasparency(true);
	m_arcs->pxSetSize(1000, 1000); //////////////////////////////////////////////////////////// for crt
								   
	m_arcs->hide();

	fixedContrast = m_paramsFile->getFloat(CFG_FIXEDCONTRAST);
	// target will include flankers as well if crowded
	targetGray = fixedContrast; 
	m_target = addObject(new CImagePlane("images/3_20x100-2.tga")); 
	m_target->replaceGray(0, targetGray);
	m_target->enableTrasparency(true);
	m_target->show();

	debug = m_paramsFile->getInteger(CFG_DEBUG);
	// this variable determines whether the experiment is run under stabilization or unstabilized
	unstab = m_paramsFile->getInteger(CFG_UNSTAB);

	// number of trials between recalibrations / fixation trials
	nRecal = m_paramsFile->getInteger(CFG_NRECAL);
	nFixat = m_paramsFile->getInteger(CFG_NFIXAT);
	nTrials = m_paramsFile->getInteger(CFG_NTRIALS);
	Conditions = m_paramsFile->getInteger(CFG_CONDITIONS);


	nHits = 0;
	nResponses = 0;

	m_flankers = {};
	if (uncrowded == 1) {
		flankerType = 0;
		flankerDist = 0; // no flankers
	}
	else {
	
		// figure out where the flankers will be positioned (xFlanker and yFlanker contain center coordinates)
		flankerType = m_paramsFile->getInteger(CFG_FLANKERTYPE);
		flankerDist = m_paramsFile->getFloat(CFG_FLANKERDIST);
		FlankerOrientations = -1; // uncrowded, will change if crowded
		float optoDim = 5.0;

		switch (flankerType) {
		case 1: // horizontal and vertical
			xFlankers = { -flankerDist, flankerDist, 0, 0 };
			yFlankers = { 0, 0, -5.f*flankerDist, 5.f*flankerDist};
			break;
		case 2: // horizontal only
			xFlankers = { -flankerDist, flankerDist};
			yFlankers = { 0, 0 };
			break;
		case 3: // vertical only
			xFlankers = { 0, 0 };
			yFlankers = { -5.f*flankerDist, 5.f*flankerDist };
			break;
		}
		for (int jj = 0; jj < xFlankers.size(); jj++) {
			m_flankers.push_back(addObject(new CImagePlane("images/3_20x100-2.tga")));
			m_flankers[jj]->hide();
			m_flankers[jj]->enableTrasparency(true);
		}

	}

	//


	// set the pixel increment for the test calibration procedure during the exp
	Increment = m_paramsFile->getInteger(CFG_STEP);
	ResponseFinalize = 0;
	xshift = 0;
	yshift = 0;
	xPos = 0;
	yPos = 0;
	TrialNumber = 1;
	m_numTestCalibration = 0;
	// set TestCalibration = 1 so that the experiment will start with 
	// a recalibration trial
	TestCalibration = 0; ///// JI: 2/23/2018 changed for testing!!!!!!!!!!!!!!!!!!!!!!!!!
	m_numCompleted = 0;

	// boxes for the recalibration trials	
	// square versions of recalibration trial markers
	m_whitecross = addObject(new CSolidPlane(255, 255, 255));
	m_whitecross->pxSetSize(60, 60); //////////////////////////////////// for crt (28)
	m_redcross = addObject(new CSolidPlane(0, 0, 0));
	m_redcross->pxSetSize(60, 60); //////////////////////////////////// for crt (28)
								  

	enable(CExperiment::EIS_PHOTOCELL);
	enable(CExperiment::EIS_NOTRACK_ICON);
	disable(CExperiment::EIS_STAT1);
	disable(CExperiment::EIS_JP_STRT);

	hideAllObjects();
	m_state = STATE_LOADING;
	m_timer.start(1000);

	WAIT_RESPONSE = 1;

	/* Seed the random-number generator with current time so that
	* the numbers will be different every time we run.*/

	srand((unsigned)time(NULL));
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::finalize()
{
	// write progress
	if (m_numCompleted > 0)//check to see that some experiments have been conducted
	{
		char LocalDate[1024];
		time_t t = time(NULL);
		strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t))
			
			;

		ostringstream fstr1;
		fstr1 << "Data\\" << m_paramsFile->getString(CFG_SUBJECT_NAME) << "\\" << m_paramsFile->getString(CFG_SUBJECT_NAME) << "Progress.txt";

		ofstream out1(fstr1.str().c_str(), ios::app);


		if (!out1.is_open())
		{

			CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Session data file (%sProgress.txt) could not be opened.  Please check that the file exists.", m_paramsFile->getString(CFG_SUBJECT_NAME));
			CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Index of last saved file: %i.", m_numSession);
			declareFinished();
		}
		// keep track of the last trial played in the list and start from there in the next session
		out1 << "Recorded: " << LocalDate << endl;
		out1 << m_numSession << endl;
		out1 << uncrowded << endl;
		out1 << TargetEccentricity << endl;
		out1 << m_numCompleted << endl;

	}
}


///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventRender(unsigned int FrameCount, CEOSData* Samples)
{
	if (TargetEccentricity == 0)
	{
		m_arcs->show();
		m_fixation->hide();
	}
	// store the stabilized trace ( --- all in px)
	else if (TargetEccentricity < 3 && TargetEccentricity > 0)
	{
		m_arcs->show();
		m_fixation->show();
	}
	else
	{
		m_arcs->hide();
		m_fixation->show();
	}
	//m_fixation->show();
	// store the stabilized trace ( --- all in px)
	if (unstab == 1)
	{
		X = 0;
		Y = 0;
	}
	else
	{
		CStabilizer::Instance()->
			stabilize(Samples, X, Y);
	}

	storeTrialStream(0, X);
	storeTrialStream(1, Y);

	// add the offset
	Xstab = X;
	Ystab = Y;
	if (unstab == 0)
	{
		X = X + xshift;
		Y = Y + yshift;
	}

	if (debug == 1)
	{
		X = 0;
		Y = 0;
	}


	float x;
	float y;
	//m_whitecross->show();

	switch (m_state) {
	case STATE_LOADING:


		COGLEngine::Instance()->clearScreen();
		glColor3d(255, 255, 255);
		//printCentered(CFontEngine::FONTS_ARIAL_18, 0, 0, "Loading...");

		// Set the background color for the experiment
		COGLEngine::Instance()->setBackgroundColor(127, 127, 127);

		// Copy the parameter file into the subject directory
		if (m_timer.isExpired())
		{
			char LocalDate[1024];
			time_t t = time(NULL);
			strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t));

			ostringstream DestinationFileName;
			DestinationFileName << m_paramsFile->getDirectory(CFG_DATA_DESTINATION) <<
				m_paramsFile->getString(CFG_SUBJECT_NAME) << "/" << m_paramsFile->getString(CFG_SUBJECT_NAME) <<
				"-" << LocalDate << "-params.cfg";;
			gotoFixation();
		}

		break;

	case STATE_TESTCALIBRATION:

		if (!m_timerCheck.isExpired())
		{
			CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
		}
		else
		{
			//CEnvironment::Instance()->outputMessage("State Test Calibration, ResponseFinalize: %.0i", ResponseFinalize);
			if (!(ResponseFinalize == 1))
			{
				m_whitecross->pxSetPosition(0, 0);
				m_whitecross->show();
				if (debug == 0)
				{
					CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
				}
				else {
					x = 0;
					y = 0;
				}
				
				m_redcross->pxSetPosition(x + xshift + xPos, y + yshift + yPos);
				//m_redcross->pxSetPosition(0, 0);

				m_redcross->show();

			}
			else
			{
				
				TestCalibration = 0;

				xshift = xPos + xshift;
				yshift = yPos + yshift;
				


				m_redcross->hide();
				m_whitecross->hide();

				endTrial();
				gotoFixation();

			}

		}
		break;

		//  STATE_FIXATION: the box appears at the center of the screen, subject is instructed to move the gaze at the center of the box,
		//                  as soon as the gaze is close to the center a fixation point appears at the center of gaze
		//                  the fixation point is stabilized and fixation should be maintained for 1 s or until the gaze is stabilized
		//                  than go to the next state.
	case STATE_FIXATION:


		storeTrialStream(0, Xstab);
		storeTrialStream(1, Ystab);

		// check if the subject is looking at the center
		//use stabilized positions acquired at the beginning of the render cycle 
		Dist = sqrt((pow(0 - (X), 2) + pow(0 - (Y), 2)));

		if (((Dist < 100) &&
			!CTriggers::any(Samples->triggers, Samples->samplesNumber, EOS_TRIG_1_BADDATA) &&
			!CTriggers::any(Samples->triggers, Samples->samplesNumber, EOS_TRIG_1_EMEVENT)) && (gate == 1) ||
			((debug == 1) && (gate == 1)))
		{
			// enter here only the first time stabilization is activated

			// this initialize stabilizer settings the first time
			//	stabilization routine is entered.
			// reset the filter with positions in arcmin
			CStabilizer::Instance()->
				resetFilter(Samples->x1, Samples->y1);
			gate = 0;
			// time for fixation ON
TimeFixationON = m_timerExp.getTime();
// fixation should stay on for one sec or as far as the gaze is stabilized
m_timerfixation.start(m_paramsFile->getFloat(CFG_FIXATION_TIME));
m_fixation->pxSetPosition(0, 0);
		}
		if (gate == 0)
		{

			// show the stabilized fixation marker with slow stabilization

			m_fixation->show();

			if (m_timerfixation.isExpired())
			{
				// if the subject is looking in the central area show the stabilized stimulus
				// hide all the objects 
				TimeFixationOFF = m_timerExp.getTime();
				hideAllObjects();
				m_target->hide();
				m_fixation->hide();
				endTrial();
				saveData();

				gotoFixation();
			}

			//CEnvironment::Instance()->outputMessage("State Cue");
		}
		break;

		// STATE_CUE: wait until subject stabilized fixation
		//            present the cue
		//            stabilize the cue for the ISI time
		//            go to the next state and present the target
	case STATE_CUE:
		//storeTrialStream(0, Xstab);
		//storeTrialStream(1, Ystab);

		// check if the subject is looking at the fixation and that there is a drift (no saccades or notrack)
		if (((gate == 0) &&
			!CTriggers::any(Samples->triggers, Samples->samplesNumber, EOS_TRIG_1_BADDATA) &&
			!CTriggers::any(Samples->triggers, Samples->samplesNumber, EOS_TRIG_1_EMEVENT)) ||
			((debug == 1) && (gate == 0)))
		{
			if (debug == 1)
			{
				X = 0;
				Y = 0;
			}
			// show the fixation
			m_fixation->show();


			// time for cue presentation
			TimeCueON = m_timerExp.getTime();

			gate = 1;
			// set the time for the cue 
			m_timercue.start(m_paramsFile->getFloat(CFG_CUE_TIME));

		}

		// when the time for the cue is over go to the next state 
		else if ((m_timercue.isExpired()) && (gate == 1))
		{

			m_state = STATE_TARGET;

			// JI: TO DO - want to keep this for non-zero eccentricities during stabilization??????????????????!!!!!!!!!!!!!!?????????
			if (abs(TargetEccentricity) <= 21) {
				m_fixation->hide();
			}

			// set the time before the target appears after the cue (based on the ISI --> take this from the parameter file)
			m_timerisi.start(m_paramsFile->getInteger(CFG_CUE_TARGET_TIME));
			gate = 0;
			TimeCueOFF = m_timerExp.getTime();

		}

		break;

		// STATE_TARGET: present the target
		//              go to the next state and wait for subject response
	case STATE_TARGET:

		//storeTrialStream(0, Xstab);
		//storeTrialStream(1, Ystab);
		// continue to stabilize the fixation marker;

		if (debug == 1)
		{
			X = 0;
			Y = 0;
		}

		if (m_timerisi.isExpired() && (gate == 0))
		{
			for (int ii = 0; ii < xFlankers.size(); ii++) {
				moveToFront(m_flankers[ii]);
			}
			moveToFront(m_target);
			TimeTargetON = m_timerExp.getTime();
			// set target duration			
			m_timertarget.start(m_paramsFile->getFloat(CFG_TARGET_TIME));

			gate = 1;
		}
		if ((gate == 1) && (m_timertarget.isExpired()))
		{ // go to the next state
			m_state = STATE_RESPONSE;
			m_target->hide();
			for (int ii = 0; ii < xFlankers.size(); ii++) {
				m_flankers[ii]->hide();
			}
			CEnvironment::Instance()->outputMessage("State Response");
			m_timerhold.start(m_paramsFile->getInteger(CFG_HOLD_TIME));
			TimeTargetOFF = m_timerExp.getTime();

		}
		else if ((gate == 1) && (!m_timertarget.isExpired()))
		{
			
			for (int ii = 0; ii < xFlankers.size(); ii++) {
				if (Conditions == 1) {
					//Both space and size change

					if (nasal == 0) {
						m_flankers[ii]->pxSetPosition(X + TargetEccentricity + xFlankers[ii] * 2 * TargetStrokewidth,
							Y + yFlankers[ii] * 2 * TargetStrokewidth);
					}
					else {
						m_flankers[ii]->pxSetPosition(X - TargetEccentricity - xFlankers[ii] * 2 * TargetStrokewidth,
							Y + yFlankers[ii] * 2 * TargetStrokewidth);
					}

					m_flankers[ii]->show();
				}
				else if (Conditions == 2) {
					//Space changes only
					m_flankers[ii]->pxSetPosition(X + TargetEccentricity + xFlankers[ii] * 2 * TargetStrokewidth,
						Y + yFlankers[ii] * 2 * TargetStrokewidth);
					m_flankers[ii]->show();
				}
				
				else if (Conditions == 3) {
					//Size changes only 
					m_flankers[ii]->pxSetPosition(X + TargetEccentricity + xFlankers[ii] * TargetStrokewidth,
						Y + yFlankers[ii]*TargetStrokewidth );

					m_flankers[ii]->show();
				}
				
			}
			// Presents targets either temporally(right) or nasally (left)
			if (nasal == 0) {
				m_target->pxSetPosition(X + TargetEccentricity, Y);
			}
			else {
				m_target->pxSetPosition(X - TargetEccentricity, Y);
			}
			m_target->show();
			moveToFront(m_target);
		}


		break;

		// STATE_RESPONSE: wait for subject response 
	case STATE_RESPONSE:

		//storeTrialStream(0, Xstab);
		//storeTrialStream(1, Ystab);

		if ((m_timerhold.isExpired()) && (WAIT_RESPONSE == 0))
		{
			// response is not given
			ResponseTime = 0;
			// hide all the objects 
			hideAllObjects();
			m_target->hide();
			m_fixation->show();
			CEnvironment::Instance()->outputMessage("No Response");
			endTrial();
			saveData();
		}
		else if (WAIT_RESPONSE == 1)
		{
			CEnvironment::Instance()->outputMessage("Got a Response!!!!!!!!");
			// hide all the objects 
			hideAllObjects();
			m_target->hide();
			m_fixation->hide();
			endTrial();
			saveData();
		}

		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventJoypad()
{
	// activate the joypad only in the state calibration	

	if (m_state == STATE_TESTCALIBRATION)
	{

		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP)) // moving the cursor up
		{
			yPos = yPos + Increment; //position of the cross
		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_DOWN)) // moving the cursor down
		{
			yPos = yPos - Increment;
		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_RGHT)) // moving the cursor to the right
		{
			xPos = xPos + Increment;

		}

		else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_LEFT)) // moving the cursor to the left
		{
			xPos = xPos - Increment;

		}

		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_Y)) // finalize the response
		{
			ResponseFinalize = 1; // click the left botton to finalize the response

		}
	}
	if (m_state == STATE_RESPONSE)
	{
		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_LEFT)) { // right = 1
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 1;
			if (TargetOrientation == 1) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}
		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_RGHT)) { // up = 2
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 2;
			if (TargetOrientation == 2) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}

		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_X)) { // left = 3
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 3;
			if (TargetOrientation == 3) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}

		if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_B)) { // down = 4
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 4;
			if (TargetOrientation == 4) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}
	}
}

void ExperimentBody::eventKeyboard(unsigned char key, int x, int y) {
	if (debug == 1 && m_state == STATE_RESPONSE) {
		//
		//
		if (key == 'd' || key == 'D') {
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 1;
			if (TargetOrientation == 1) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}
		else if (key == 'w' || key == 'W') {
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 2;
			if (TargetOrientation == 2) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}
		else if (key == 'a' || key == 'A') {
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 3;
			if (TargetOrientation == 3) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}
		else if (key == 's' || key == 'S') {
			WAIT_RESPONSE = 1;
			ResponseTime = m_timerExp.getTime();
			Response = 4;
			if (TargetOrientation == 4) {
				Correct = 1;
			}
			else {
				Correct = 0;
			}
		}
	}
}


/********** goToFixation ********/
// initilizes the trial parameters then moves into fixation state
void ExperimentBody::gotoFixation() {

	//CEOS::Instance()->setVideoOutputDirectory("D:\\exp\\\Ashley\\fovealcrowdingcpp_ddpi\\Data\\Video\\"); 
	//Both size and space change 
	if(Conditions == 1){
		CEnvironment::Instance()->outputMessage("going to fixation %i", TestCalibration);
		if (!(TestCalibration == 1))
			CEnvironment::Instance()->outputMessage("Trial Number: %d", (TrialNumber));

		isPest = m_paramsFile->getFloat(CFG_ISPEST);
		fixedTargetStrokewidth = m_paramsFile->getFloat(CFG_FIXEDTARGETSTROKEWIDTH);

		hideAllObjects();


		if (TestCalibration == 1)
		{
			m_state = STATE_TESTCALIBRATION;
			//CEnvironment::Instance()->outputMessage("Calibration trial");
			ResponseFinalize = 0;
			m_timerCheck.start(500);
		}
		else
		{
			// if no response is delivered, the corrent and response variables are left at 3
			Correct = 3;
			Response = 1000;
			pestLevel = -1;
			ResponseTime = 0;
			WAIT_RESPONSE = 0;

			TimeCueON = 0; // state hold
			TimeCueOFF = 0;
			TimeFixationON = 0;
			TimeFixationOFF = 0;
			TimeTargetON = 0;
			TimeTargetOFF = 0;

			if (TrialNumber % nFixat == 0) {
				m_state = STATE_FIXATION;
				m_fixation->setColor(0, 0, 0);

				gate = 1;
				Beep(1000, 400);
			}
			else {
				m_state = STATE_CUE;
				gate = 0;
				m_fixation->setColor(255, 255, 255);

				TargetOrientation = (rand() % 4) + 1; // randomly select an orientation (LRUD)
				//determine the size
				pestLevel = pest->getTestLvl();

				//if (pestLevel - floor(pestLevel) > 0) {
				//	CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Fractional stroke width value %f", pestLevel);
				//	declareFinished();
				//}

				if (isPest == 1)
				{
					if (pestLevel - floor(pestLevel) > 0)
					{
						CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Fractional stroke width value %f", pestLevel);
						declareFinished();
					}
					TargetStrokewidth = pestLevel;
				}
				else
				{
					TargetStrokewidth = fixedTargetStrokewidth;
				}

				//TargetStrokewidth = pestLevel;

				CEnvironment::Instance()->outputMessage("Eccentricity: %i    Orientation: %i     Strokewidth: %i", TargetEccentricity, TargetOrientation, TargetStrokewidth);

				switch (TargetOrientation) {
				case 1:  ///// JI: TO DO - replace with real image names
					m_target->loadImage("images/3_20x100-2.tga");
					break;
				case 2:
					m_target->loadImage("images/5_20x100-2.tga");
					break;
				case 3:
					m_target->loadImage("images/6_20x100-2.tga");
					break;
				case 4:
					m_target->loadImage("images/9_20x100-2.tga");
					break;
				}
				//m_target->setColor(EIS_RGB(fixedContrast, fixedContrast, fixedContrast)); 
				m_target->pxSetSize(2 * TargetStrokewidth, 10 * TargetStrokewidth);
				m_target->replaceGray(0, targetGray);
				m_target->pxSetPosition(TargetEccentricity, 0);
				m_target->hide();



				if (uncrowded == 0) {
					int fo;
					FlankerOrientations = 0;

					for (int ii = 0; ii < xFlankers.size(); ii++) {
						fo = (rand() % 4) + 1;
						while (fo == TargetOrientation) //flankers will not match the target
						{
							fo = (rand() % 4) + 1;
						}
						
						FlankerOrientations = 10 * FlankerOrientations + fo; //save the orientation, no actual effect on the experiment

						switch (fo) {
						case 1:
							m_flankers[ii]->loadImage("images/3_20x100-2.tga");
							break;
						case 2:
							m_flankers[ii]->loadImage("images/5_20x100-2.tga");
							break;
						case 3:
							m_flankers[ii]->loadImage("images/6_20x100-2.tga");
							break;
						case 4:
							m_flankers[ii]->loadImage("images/9_20x100-2.tga");
							break;
						}
						if (nasal == 0) {
							m_flankers[ii]->pxSetPosition(TargetEccentricity + xFlankers[ii] * 2 * TargetStrokewidth,
								yFlankers[ii] * 2 * TargetStrokewidth);

						}
						else {
							m_flankers[ii]->pxSetPosition(-TargetEccentricity - xFlankers[ii] * 2 * TargetStrokewidth,
								yFlankers[ii] * 2 * TargetStrokewidth);

						}

						m_flankers[ii]->pxSetSize(2 * TargetStrokewidth, 10 * TargetStrokewidth);
						//m_flankers[ii]->setColor(EIS_RGB(fixedContrast, fixedContrast, fixedContrast)); 
						//m_flankers[ii]->pxSetPosition(TargetEccentricity + xFlankers[ii] * 2 * TargetStrokewidth,
							//yFlankers[ii] * 2 * TargetStrokewidth);
						m_flankers[ii]->replaceGray(0, targetGray);
						m_flankers[ii]->hide();
					}
					
				}
				//change the arcs when changes the stimuli 
					m_arcs->pxSetSize(50 * TargetStrokewidth, 50 * TargetStrokewidth);
			}
		}

	
	}
	//Space change only 
	else if (Conditions == 2) {
		CEnvironment::Instance()->outputMessage("going to fixation %i", TestCalibration);
		if (!(TestCalibration == 1))
			CEnvironment::Instance()->outputMessage("Trial Number: %d", (TrialNumber));


		hideAllObjects();
	

		if (TestCalibration == 1)
		{
			m_state = STATE_TESTCALIBRATION;
			//CEnvironment::Instance()->outputMessage("Calibration trial");
			ResponseFinalize = 0;
			m_timerCheck.start(500);
		}
		else
		{
			// if no response is delivered, the corrent and response variables are left at 3
			Correct = 3;
			Response = 1000;
			pestLevel = -1;
			ResponseTime = 0;
			WAIT_RESPONSE = 0;

			TimeCueON = 0;
			TimeCueOFF = 0;
			TimeFixationON = 0;
			TimeFixationOFF = 0;
			TimeTargetON = 0;
			TimeTargetOFF = 0;

			if (TrialNumber % nFixat == 0) {
				m_state = STATE_FIXATION;
				m_fixation->setColor(0, 0, 0);

				gate = 1;
				Beep(1000, 400);
			}
			else {
				m_state = STATE_CUE;
				gate = 0;
				m_fixation->setColor(255, 255, 255);

				TargetOrientation = (rand() % 4) + 1; // randomly select an orientation (LRUD)
				pestLevel = pest->getTestLvl();

				if (pestLevel - floor(pestLevel) > 0) {
					CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Fractional stroke width value %f", pestLevel);
					declareFinished();
				}
				TargetStrokewidth = pestLevel;

				CEnvironment::Instance()->outputMessage("Eccentricity: %i    Orientation: %i     Strokewidth: %i", TargetEccentricity, TargetOrientation, TargetStrokewidth);

				switch (TargetOrientation) {
				case 1:  ///// JI: TO DO - replace with real image names
					m_target->loadImage("images/3_20x100-2.tga");
					break;
				case 2:
					m_target->loadImage("images/5_20x100-2.tga");
					break;
				case 3:
					m_target->loadImage("images/6_20x100-2.tga");
					break;
				case 4:
					m_target->loadImage("images/9_20x100-2.tga");
					break;
				}
				//size does not change
				
				m_target->replaceGray(0, targetGray);
				m_target->pxSetPosition(TargetEccentricity, 0);
				m_target->hide();



				if (uncrowded == 0) {
					int fo;
					FlankerOrientations = 0;

					for (int ii = 0; ii < xFlankers.size(); ii++) {
						fo = (rand() % 4) + 1;
						FlankerOrientations = 10 * FlankerOrientations + fo;

						switch (fo) {
						case 1:
							m_flankers[ii]->loadImage("images/3_20x100-2.tga");
							break;
						case 2:
							m_flankers[ii]->loadImage("images/5_20x100-2.tga");
							break;
						case 3:
							m_flankers[ii]->loadImage("images/6_20x100-2.tga");
							break;
						case 4:
							m_flankers[ii]->loadImage("images/9_20x100-2.tga");
							break;
						}

						
						m_flankers[ii]->pxSetPosition(TargetEccentricity + xFlankers[ii] * 5 * TargetStrokewidth,
							yFlankers[ii] * 5 * TargetStrokewidth);
						m_flankers[ii]->replaceGray(0, targetGray);
						m_flankers[ii]->hide();
					}
					//change the arcs when changes the stimuli 
					m_arcs->pxSetSize(50 * TargetStrokewidth, 50 * TargetStrokewidth);
				}
			}
		}

	}//Size changes only
	else if (Conditions == 3){
		CEnvironment::Instance()->outputMessage("going to fixation %i", TestCalibration);
		if (!(TestCalibration == 1))
			CEnvironment::Instance()->outputMessage("Trial Number: %d", (TrialNumber));


		hideAllObjects();


		if (TestCalibration == 1)
		{
			m_state = STATE_TESTCALIBRATION;
			//CEnvironment::Instance()->outputMessage("Calibration trial");
			ResponseFinalize = 0;
			m_timerCheck.start(500);
		}
		else
		{
			// if no response is delivered, the corrent and response variables are left at 3
			Correct = 3;
			Response = 1000;
			pestLevel = -1;
			ResponseTime = 0;
			WAIT_RESPONSE = 0;

			TimeCueON = 0;
			TimeCueOFF = 0;
			TimeFixationON = 0;
			TimeFixationOFF = 0;
			TimeTargetON = 0;
			TimeTargetOFF = 0;

			if (TrialNumber % nFixat == 0) {
				m_state = STATE_FIXATION;
				m_fixation->setColor(0, 0, 0);

				gate = 1;
				Beep(1000, 400);
			}
			else {
				m_state = STATE_CUE;
				gate = 0;
				m_fixation->setColor(255, 255, 255);

				TargetOrientation = (rand() % 4) + 1; // randomly select an orientation (LRUD)
				pestLevel = pest->getTestLvl();

				if (pestLevel - floor(pestLevel) > 0) {
					CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Fractional stroke width value %f", pestLevel);
					declareFinished();
				}
				TargetStrokewidth = pestLevel;

				CEnvironment::Instance()->outputMessage("Eccentricity: %i    Orientation: %i     Strokewidth: %i", TargetEccentricity, TargetOrientation, TargetStrokewidth);

				switch (TargetOrientation) {
				case 1:  ///// JI: TO DO - replace with real image names
					m_target->loadImage("images/3_20x100-2.tga");
					break;
				case 2:
					m_target->loadImage("images/5_20x100-2.tga");
					break;
				case 3:
					m_target->loadImage("images/6_20x100-2.tga");
					break;
				case 4:
					m_target->loadImage("images/9_20x100-2.tga");
					break;
				}
				m_target->pxSetSize(2 * TargetStrokewidth, 10 * TargetStrokewidth);
				m_target->replaceGray(0, targetGray);
				m_target->pxSetPosition(TargetEccentricity, 0);
				m_target->hide();




				if (uncrowded == 0) {
					int fo;
					FlankerOrientations = 0;

					for (int ii = 0; ii < xFlankers.size(); ii++) {
						fo = (rand() % 4) + 1;
						FlankerOrientations = 10 * FlankerOrientations + fo;
						
						switch (fo) {
						case 1:
							m_flankers[ii]->loadImage("images/3_20x100-2.tga");
							break;
						case 2:
							m_flankers[ii]->loadImage("images/5_20x100-2.tga");
							break;
						case 3:
							m_flankers[ii]->loadImage("images/6_20x100-2.tga");
							break;
						case 4:
							m_flankers[ii]->loadImage("images/9_20x100-2.tga");
							break;
						}



						m_flankers[ii]->pxSetSize(2 * TargetStrokewidth, 10 * TargetStrokewidth);
						m_flankers[ii]->pxSetPosition(TargetEccentricity + xFlankers[ii],
							yFlankers[ii]);
						m_flankers[ii]->replaceGray(0, targetGray);
						m_flankers[ii]->hide();
					}
				}
				//change the arcs when changes the stimuli 
				m_arcs->pxSetSize(50 * TargetStrokewidth, 50 * TargetStrokewidth);
			}
		}

	}


	


	// start the trial	
	CEnvironment::Instance()->outputMessage("starting trial");
	//startTrial();
	startTrial(true, true, false); //set last image to true if you want video recorded, otherwise set to false
	m_timerExp.start();
	m_timer.start(1000);
}



/******* saveData: save trial variables and write them to some kind of output file ********/
void ExperimentBody::saveData() {
	if (ResponseTime > 0)
		// give confrimation of response
		Beep(600, 400);

	// time of the response (locked to the start of the trial)
	storeTrialVariable("ResponseTime", ResponseTime);
	storeTrialVariable("TimeCueON", TimeCueON);
	storeTrialVariable("TimeCueOFF", TimeCueOFF);
	storeTrialVariable("TimeFixationON", TimeFixationON);
	storeTrialVariable("TimeFixationOFF", TimeFixationOFF);
	storeTrialVariable("TimeTargetON", TimeTargetON);
	storeTrialVariable("TimeTargetOFF", TimeTargetOFF);
	storeTrialVariable("Correct", Correct);
	storeTrialVariable("TargetEccentricityIndex", TargetEccentricityIndex);
	storeTrialVariable("TargetOrientation", TargetOrientation);
	storeTrialVariable("TargetEccentricity", TargetEccentricity);
	storeTrialVariable("Uncrowded", uncrowded);
	storeTrialVariable("TargetStrokewidth", TargetStrokewidth);
	storeTrialVariable("PestLevel", pestLevel);
	storeTrialVariable("Response", Response);
	storeTrialVariable("Unstab", unstab);
	storeTrialVariable("Subject", m_paramsFile->getString(CFG_SUBJECT_NAME));
	storeTrialVariable("FlankerType", flankerType);
	storeTrialVariable("FlankerDist", flankerDist);
	storeTrialVariable("FixationSize", m_paramsFile->getInteger(CFG_FIXATIONSIZE));
	storeTrialVariable("FlankerOrientations", FlankerOrientations);
	storeTrialVariable("TargetGray", targetGray);
	storeTrialVariable("InitPestLevel", initLevel);
	storeTrialVariable("SessionNumber", m_numSession);
	//storeTrialVariable("XResolution", m_paramsFile->getInteger(CFG_X_RESOLUTION));
	storeTrialVariable("ScreenDistance", m_paramsFile->getInteger(CFG_SCREEN_DISTANCE));
	storeTrialVariable("pixelAngle0", pixelAngle0);
	storeTrialVariable("pixelAngle", pixelAngle);
	storeTrialVariable("magnificationFactor", magnificationFactor);
	storeTrialVariable("nasal", nasal);
	storeTrialVariable("isPest", isPest);
	storeTrialVariable("fixedContrast", fixedContrast);
	storeTrialVariable("fixedTargetStrokewidth", fixedTargetStrokewidth);


	//storeTrialVariable("screenDist", m_paramsFile->getInteger(CFG_SCREEN_DIST));
	//storeTrialVariable("magnificationFactor", magnificationFactor);
	storeTrialVariable("ScreenWidth", m_paramsFile->getInteger(CFG_SCREEN_WIDTH));
	//the new parameter for different conditions
	storeTrialVariable("Conditions", Conditions);

	//isPest = m_paramsFile->getFloat(CFG_ISPEST);
	if (Correct < 3) {
		pest->addTrial(Correct);
		nHits += Correct;
		nResponses += 1;
		int hits = pest->getHits();
		int trials = pest->getTrials();
		//	float perf = float(hits) / (float(trials)); commented out so that I get overall performance, not just within the 3 pest trials
			float perf = float(nHits) / (float(nResponses));

		m_numCompleted++;
		CEnvironment::Instance()->outputMessage("Target: %i   Response: %i   Correct? %i     Total Perf %f", TargetOrientation, Response, Correct, perf);
	}
	// 
	storeTrialVariable("Subject_Name", m_paramsFile->getString(CFG_SUBJECT_NAME));

	// save information about the test calibration
	storeTrialVariable("TestCalibration", TestCalibration);
	storeTrialVariable("xoffset", xshift);
	storeTrialVariable("yoffset", yshift); //px

// if (CFG_UNCROWDED = 1) {
// condition = "Uncrowded"
// }
// if (CFG_UNCROWDED = 0) {}
// condition = "Crowded"
// }
// if(CFG_UNSTAB = 1) {
// 	stabil = "Unstabilized"
// }
// if (CFG_UNSTAB = 0){
// 	stabil = "Stabilized"
// }

// if(CFG_NASAL = 1){
// 	nameEcc = "nasal"
// }
// if(CFG_NASAL = 0){
// 	nameEcc = "temp"
// }

//  newFolderName = CEnvironment::Instance()->outputMessage("%s_%s_%iecc%s", condition, stabil, name, CFG_TARGET_ECCENTRICITY, nameEcc);

// 		if (!fs::is_directory("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME)) || !fs::exists("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME))) { // Check if src folder exists
//     	fs::create_directory("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME)); // create src folder
// 		}

	
	saveTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME));
	saveDebugTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT_NAME));


	// keep track of the test calibration trials
	m_numTestCalibration++;
	// recalibration active at every nRecal trials
	if (m_numTestCalibration == nRecal)
	{
		xPos = 0;
		yPos = 0;
		TestCalibration = 1;
		ResponseFinalize = 0;
		m_numTestCalibration = 0;
		m_timerCheck.start(100);
		m_whitecross->pxSetPosition(0, 0);
		m_whitecross->show();
	}

	if (TrialNumber >= nTrials) {
		declareFinished();
	}

	TrialNumber++;

	CEnvironment::Instance()->outputMessage("-----------------------------------------------------");
	gotoFixation();
}


///////////////////////////////////////////////////////////////////////////////////
/******* loadSession: read all trial parameters from a stimulus list ********/
void ExperimentBody::loadSession() {
	//read eccentricities from EccentricityList
	ostringstream fstr1;
	fstr1 << "Data\\" << m_paramsFile->getString(CFG_SUBJECT_NAME) << "\\" << m_paramsFile->getString(CFG_SUBJECT_NAME) << "_itinerary.txt";

	string tempStr, discard;
	int tempInt1, tempInt2, tempInt3;
	ifstream in1(fstr1.str().c_str());

	if (!in1.is_open())
	{
		CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Session data file (%s_itinerary.txt) could not be opened.  Please check that the file exists.", m_paramsFile->getString(CFG_SUBJECT_NAME));
		declareFinished();

	}

	// the first number on each sequence in the StimList indicates the trial number
	// the second number indicates cue direction
	// the third number indicats whether the cue is valid invalid or neutral
	// (valid = 1 invalid = 0 neutral = 2 catch = 3)

	while (in1.peek() != EOF)
	{
		in1 >> discard;  // discard *
		in1 >> tempInt1; // session #
		in1 >> tempInt2; // uncrowded or crowded  if tempint2 == 0 crowded
		in1 >> tempInt3; // eccentricity

		//if (tempInt1 == m_numSession) {
		//	break;
		//}
	}
	//SET CROWDED PARAMS HERE: crowded = 0, uncrowded = 1
	//uncrowded = 1;
	//TargetEccentricity = 0;

	

	//CEnvironment::Instance()->outputMessage("Starting Session: %i    Uncrowded: %i     Eccentricity: %i", m_numSession, uncrowded, TargetEccentricity);

	// pest values
	float pestStep = 4; //////////////////////////////////// for crt (4)
	float waldConstant = 1; // defines boundary around desired target (in trials)
	float targetP = 0.62; // use 62% for 4-afc task

						  /* PEST initialization
						  for now let's just start with a new pest object */
	pest = new Pest(initLevel, targetP, pestStep, waldConstant, 0);
}

/******* loadProgress: pick up where we left off from last session ********/
void ExperimentBody::loadProgress() {
	//read in progress from [SubjectName]Progress.txt
	ostringstream fstr1;
	fstr1 << "Data\\" << m_paramsFile->getString(CFG_SUBJECT_NAME) << "\\" << m_paramsFile->getString(CFG_SUBJECT_NAME) << "Progress.txt";

	string tempStr1;
	string discard;
	int tempInt1, discardInt;
	ifstream in1(fstr1.str().c_str());

	if (!in1.is_open())
	{
		CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Progress data file (%sProgress.txt) could not be opened for writing.  Please check that the file exists.", m_paramsFile->getString(CFG_SUBJECT_NAME));
		declareFinished();
	}

	bool empty = true;

	while (in1.peek() != EOF)
	{
		in1 >> discard; //discard date line
		in1 >> tempInt1;
		in1 >> discardInt; // discard previous uncrowded/crowded
		in1 >> discardInt; // discard previous eccentricity value
		in1 >> discardInt; // discard number of previously completed trials
		empty = false;
	}

	if (empty == false)
	{
		m_numSession = tempInt1 + 1;
	}
	else
	{
		m_numSession = 1;
	}

	CEnvironment::Instance()->outputMessage("Starting Session: %i", m_numSession);

}

