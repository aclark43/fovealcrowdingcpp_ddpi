#include "stdafx.h"
#include <emil/CUtilities.hpp>
#include "CVertexObjectExceptions.hpp"
///////////////////////////////////////////////////////////////////////////////////////////////
xVertex::xVertex(errorCode_t code, const std::string& func, const std::string& arg):
    xBase(code, func),
    m_arg(arg)
{}
///////////////////////////////////////////////////////////////////////////////////////////////
std::string xVertex::_what() const
{
    switch (getCode()) {
        case ERR_SIZE_NOT_MATCH:
			return "Size of vertices not match.";
		case ERR_SIZE_TOO_LESS:
			return "Size of vertices less than three.";
		case ERR_OGL:
			return "OpenGL error: \"" + m_arg + "\"";
        default:                                
			return nastyMessage();
    }
}
