#ifndef _VECTORS_H
#define _VECTORS_H

#include <vector>
#include <string>

typedef std::vector<int> integervector_t;
typedef std::vector<std::string> stringvector_t;
typedef std::vector<int> SubjectSequence_t;

integervector_t randomVector(int Size);
//void createRandomVector(std::string File
void saveIntegerVector(std::string FileName, integervector_t Vector);
integervector_t loadIntegerVector(std::string FileName);
stringvector_t loadStringVector(std::string FileName);



#endif