#ifndef _CSolidGratingEXCEPTIONS_H
#define _CSolidGratingEXCEPTIONS_H
#include <emil/libc_common.hpp>
#include <emil/CQueryable.hpp>
#include <emil/CExceptions.hpp>
///////////////////////////////////////////////////////////////////////////////////////////////
/// Image-related exception
class xVertex: public xBase
{
public:
	xVertex(errorCode_t code, const std::string& func, const std::string& arg = "") throw();

    enum {
        ERR_SIZE_NOT_MATCH,    /** Unable to open image file */
		ERR_SIZE_TOO_LESS,
		ERR_OGL,
    };

protected:
    std::string _what() const throw();
    std::string _getHeader() const throw()
    { return "Image exception"; }

private:
    std::string m_arg;
};
#endif    // _CSolidGratingEXCEPTIONS_H
