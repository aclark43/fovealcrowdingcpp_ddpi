Step-by-step instructions for experimenter

PRELIMINARY ADJUSTEMENTS
1. Turn on the rigth eyetracker ~20 minutes before starting the experiment
2. Place the monitor (ROG 258 labeled small ASUS on  -- see white label at the top of the monitor --- sitting on the light brown table)
   in the correct position. The monitor needs a special cable to connect to the computer because it requires 200 Hz. 
3. Set up two mirrors that will be used in the experiment. The first one is in front of the eyetracker on the eyetracker table where the metal bar is (check for the intersection of the blue tape).
	The second one should be set on the table which is on the right of the eyetracker. Have to adjust the angle of the mirrors so that subjects can see the reflection of the monitor
    at the center of their visions.
3. Confirm that the monitor is at the correct location using the laser meter, the distance should be approximately 3650 mm from the 
   eye of the observer. 
4. Make sure that the center of the monitor is aligned with the subject's straight ahead, so that stimuli at the center of the display are not viewed with the eye at an angle.
5. Make sure the monitor is centered and does not have any tilt. Use the leveler for checking the tilt and align the wheels of the table on the blue strip on the
floor. Measure the distance with the laser meter at the four corners of the monitor (the distance is 3650mm at all the corners).
6. Turn off any other monitor and turn on the experiment pc (pwd Qebgcsft2010)
7. Click on the Nvidia control panel icon at the bottom of the monitor on the icon tray (small green and black icon with a spiral)
8. Once the Nvidia panel opens, select the option on the left "Set up multiple display" and make sure that the two selected monitors in the 
   panel "Select the dysplay you want to use" are - Ancor Communication Inc VS248 and ROG PG258Q(G-SYNC Capable). 
9. Right click on the screen with 2 and make it primary amd select apply.  Gamma correction is set at 1. In the Nvidia panel select "Adjust desktop color settings" 
   Select "Use NVIDIA settings" and then select "Gamma" and "AllChannels" at the top and The number next to gamma should be set equal to 1. Select Apply.
10. In the NVIDIA control panel go to "Change Resolution" and set the resolution of the display at 1920 x 1080. SELECT APPLY - then YES. 
11. Using the controls on the back of the ROG monitor make sure that the brightness and contrast of the display are set at 0
12. Open the folder: D:\Janis\FovealAcuity-PelliFont
13. Open the file: FovealAcuity-PelliFont.vcxproj
14. Click on build on the top left of the screen and select rebuild solution
15. If the console at the bottom does not report any error you can proceed
16. Hit F5 to run the experiment (make sure the DSP is on)

SETTING UP A NEW SUBJECT
1. make a copy of the folder "D:\Janis\FovealAcuity-PelliFont\Data\Test"
2. Change the name of the copied folder with the subject name
3. open the new folder and change the progress txt file from "TestNameProgress.txt" and put the name of the subject in place of Test
4. Open the params.cfg file (D:\Janis\FovealAcuity-PelliFont\Params.cfg) 
5. In the params file change the name of the subject and the other relevant variables.
6. Delete all EIS2- files that pertain to "Test" 
7. Change "Test_itinerary" by removing "Test" and putting the Subject name
8. Change "TestProgress" by removing "Test" and putting the Subject name
NOTE: if the subject has never been in the experiment before give clear instructions and start with an unstabilized uncrowded stimulus, longer presentation time if necessary. Have the subject view the images of each stimulus before hand.


Make sure the subject understand the recalibration procedure and is careful in adjusting the offset. The subject needs some time to understand
the recalibration procedure and perform it carefully.
Make sure that the subject mainteins the gaze stable and does not chase with the eyes the stimulus when it is presented under retinal 
stabilization. 
Make sure the subject knows how the target looks like in crowded condition and konws which buttons represent the number. 
Make sure the subject has to deliver the response by pressing the joypad once the stimulus is off the display


NOTE: Data are saved in the Subject folder but once the experiment session is finished you should place them in a subfolder with a meaningful label based on the
condition you run

--------------------------------------------
ATTENTION:

when changing screen setting and viewing distance make sure to change the following parameters in the Monitor Configuration file (emil-library-monocular-singleROG):

Distance = 3650

# Width of the screen (in mm)
Width = 

# Height of the screen (in mm)
Height = 

# Initial refresh rate and resolution of the welcome screen
H-Resolution = 1920 
V-Resolution = 1080
Refresh-Rate = 200






