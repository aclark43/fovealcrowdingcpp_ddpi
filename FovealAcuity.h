
// FovealAcuity.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include <emil/emil.hpp>

const std::string CFG_SUBJECT_NAME = "Subject";
const std::string CFG_TARGET_ECCENTRICITY = "TargetEccentricity";
const std::string CFG_UNCROWDED = "uncrowded";
const std::string CFG_STEP = "Step";
const std::string CFG_DATA_DESTINATION = "DataDestination";
const std::string CFG_UNSTAB = "Unstab";
const std::string CFG_FLANKERTYPE = "FlankerType";
const std::string CFG_FLANKERDIST = "FlankerDist";
const std::string CFG_FIXATIONSIZE = "FixationSize";
const std::string CFG_DEBUG = "debug";
const std::string CFG_FIXATION_SIZE = "FixationSize";
const std::string CFG_FIXATION_TIME = "FixationTime";
const std::string CFG_CUE_TIME = "CueTime";
const std::string CFG_CUE_TARGET_TIME = "CueTargetTime";
const std::string CFG_HOLD_TIME = "HoldTime";
const std::string CFG_TARGET_TIME = "TargetTime";
const std::string CFG_PESTINIT = "PestInit";
const std::string CFG_NTRIALS = "NTrials";
const std::string CFG_NRECAL = "NRecal";
const std::string CFG_NFIXAT = "NFixat";
const std::string CFG_X_RESOLUTION = "X_res";
const std::string CFG_Y_RESOLUTION = "Y_res";
const std::string CFG_SCREEN_WIDTH = "ScreenWidth";
const std::string CFG_SCREEN_DISTANCE = "ScreenDistance";
const std::string CFG_MAGFACTOR = "magFactor";
//const std::string CFG_SCREEN_DIST = "screenDist"; //06142019 AMC
const std::string CFG_REFRESH_RATE = "RefreshRate";
//new variable
const std::string CFG_CONDITIONS = "Conditions";
const std::string CFG_NASAL = "nasal";
const std::string CFG_ISPEST = "isPest";
const std::string CFG_FIXEDCONTRAST = "fixedContrast";
const std::string CFG_FIXEDTARGETSTROKEWIDTH = "fixedTargetStrokewidth";



// FovealAcuityApp:
// See FovealAcuity.cpp for the implementation of this class
//

class CFovealAcuityApp : public CWinApp
{
public:
	CFovealAcuityApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	
	// Configuration file
	CCfgFile m_paramsFile;
};

extern CFovealAcuityApp theApp;
