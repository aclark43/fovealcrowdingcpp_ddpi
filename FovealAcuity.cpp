
// FovealAcuity.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FovealAcuity.h"
#include "ExperimentBody.h"

#include "myManualCalibrator2.h"
#include "myAutoCalibration.h"
#include <ExpDDPICalibration.hpp>
#include <emil2-console\CWinEMIL.hpp>
#include <emil2-console\CEMILConsole.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CEyeChartEApp

BEGIN_MESSAGE_MAP(CFovealAcuityApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CEyeChartEApp construction

CFovealAcuityApp::CFovealAcuityApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	m_paramsFile.addVariable(CFG_SUBJECT_NAME, std::string(""));
	m_paramsFile.addVariable(CFG_TARGET_ECCENTRICITY, 5.0f);
	m_paramsFile.addVariable(CFG_UNCROWDED, 1);
	m_paramsFile.addVariable(CFG_DATA_DESTINATION, std::string(""));
	m_paramsFile.addVariable(CFG_STEP, 5.0f);
	m_paramsFile.addVariable(CFG_UNSTAB, 5.0f);
	m_paramsFile.addVariable(CFG_FLANKERTYPE, 5.0f);
	m_paramsFile.addVariable(CFG_FLANKERDIST, 5.0f);
	m_paramsFile.addVariable(CFG_FIXATIONSIZE, 1);
	m_paramsFile.addVariable(CFG_FIXATION_TIME, 5.0f); 
	m_paramsFile.addVariable(CFG_CUE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_CUE_TARGET_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_HOLD_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_TARGET_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_PESTINIT, 5.0f);
	m_paramsFile.addVariable(CFG_DEBUG, 1);
	m_paramsFile.addVariable(CFG_NTRIALS, 1);
	m_paramsFile.addVariable(CFG_NFIXAT, 1);
	m_paramsFile.addVariable(CFG_NRECAL, 1);
	m_paramsFile.addVariable(CFG_FIXATION_SIZE, 1);
	m_paramsFile.addVariable(CFG_X_RESOLUTION, 1);
	m_paramsFile.addVariable(CFG_Y_RESOLUTION, 1);
	m_paramsFile.addVariable(CFG_SCREEN_DISTANCE, 1);
	m_paramsFile.addVariable(CFG_MAGFACTOR, 1.0f);
	m_paramsFile.addVariable(CFG_SCREEN_WIDTH, 1);
	m_paramsFile.addVariable(CFG_REFRESH_RATE, 150);
	m_paramsFile.addVariable(CFG_CONDITIONS, 1);
	m_paramsFile.addVariable(CFG_NASAL, 1);
	m_paramsFile.addVariable(CFG_ISPEST, 1);
	m_paramsFile.addVariable(CFG_FIXEDCONTRAST, 1);
	m_paramsFile.addVariable(CFG_FIXEDTARGETSTROKEWIDTH, 1);
}


// The one and only CEyeChartEApp object

CFovealAcuityApp theApp;


// CEyeChartEApp initialization

BOOL CFovealAcuityApp::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	CWinApp::InitInstance();
	AfxEnableControlContainer();
	CShellManager *pShellManager = new CShellManager;
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
	SetRegistryKey(_T("EyeRIS @ APLab"));

	// EyeRIS ------------------------------------------------------------------
	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	CWinEMIL::Instance()->initialize("./MonitorConfiguration/emil-library-monocular-singleROG.cfg");
	// Put your code here ======================================================

	// Load the specified configuration file
	m_paramsFile.loadFile("params.cfg");
	
	std::string DestinationDir = m_paramsFile.getDirectory(CFG_DATA_DESTINATION) +
		m_paramsFile.getString(CFG_SUBJECT_NAME);// + "/calibration";

	int HResolution = m_paramsFile.getInteger(CFG_X_RESOLUTION);
	int VResolution = m_paramsFile.getInteger(CFG_Y_RESOLUTION);
	int RefreshRate = m_paramsFile.getInteger(CFG_REFRESH_RATE);
	//int screenDist = m_paramsFile.getInteger(CFG_SCREEN_DIST); //06142019 AMC
	int ScreenDistance = m_paramsFile.getInteger(CFG_SCREEN_DISTANCE);

	CEnvVariables::Instance()->addVariable(CFG_N_SCREEN_DISTANCE, ScreenDistance); //06142019 AMC
	//CEnvVariables::Instance()->addVariable(CFG_SCREEN_DIST, screenDist);//06142019 AMC

	
	bool badal = false; //updown`	
	bool mirror = true; //leftright = if badal AND mirror "should" be true, set badal to true and mirror to false


	if (m_paramsFile.getInteger(CFG_DEBUG) == 0) {
		ExpDDPICalibration* pexp2 = new ExpDDPICalibration(HResolution, VResolution, RefreshRate);
		pexp2->setGridDotSize(60);
		pexp2->setBackground(EIS_RGB(0, 0, 0));
		pexp2->setGridColor(EIS_RGB(255, 255, 255));
		CWinEMIL::Instance()->addExperiment(pexp2);

		ExAutoCalibrator2* autoCalibrator = new ExAutoCalibrator2(HResolution, VResolution, RefreshRate);
		autoCalibrator->setOutputDir(DestinationDir);// +"/calibration");
		autoCalibrator->setImageReversed(mirror, badal);
		autoCalibrator->setGridPointSize(60);
		autoCalibrator->setBackgroundColor(0, 0, 0);

		//autoCalibrator->setPointOffset(150, 150);
		CWinEMIL::Instance()->addExperiment(autoCalibrator);

		ExManualCalibrator2* manualCalibrator = new ExManualCalibrator2(HResolution, VResolution, RefreshRate);
		manualCalibrator->setOutputDir(DestinationDir);// +"/calibration");
		manualCalibrator->setImageReversed(mirror, badal);
		manualCalibrator->setGridPointSize(60);
		manualCalibrator->setBackgroundColor(0, 0, 0);
		CWinEMIL::Instance()->addExperiment(manualCalibrator);
	}


	ExperimentBody* pexp = new ExperimentBody(HResolution, VResolution, RefreshRate, &m_paramsFile);
	pexp->setImageReversed(mirror, badal);
	CWinEMIL::Instance()->addExperiment(pexp);

	// =========================================================================
	//CEyeChartEDlg dlg;
	CEMILConsole dlg("emil-console-monocular-singleROG.cfg");
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	 
	// Destroy the the library
	CWinEMIL::Destroy();
	// EyeRIS ------------------------------------------------------------------
	
	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

